
(** A set of Z80 registers *)
type t

val init_registers : t

val switch_registers : t * t -> t * t
