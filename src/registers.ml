
type register_set = {
    af : int;
}

type t = register_set

let init_registers =
  { af = 0; }

let switch_registers (t, t1) = (t1, t)
